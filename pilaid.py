counter = 0
class NodoPila:
  def __init__(self, valor, token, linea, var_type, scope , id_type, asignacion = "") -> None:
    global counter
    self.id = counter
    self.valor = valor
    self.token = token
    self.linea = linea
    self.var_type = var_type
    self.scope = scope
    self.asignacion = asignacion
    self.id_type = id_type
    self.next = None

    counter += 1

class Pila:
  def __init__(self) -> None:
    self.sup = None
    
  def push(self, id, valor, token, linea, var_type, scope, id_type):
    if self.sup == None:
      self.sup = NodoPila(id, valor, token, linea, var_type, scope, id_type)
      return

    nuevo_nodo = NodoPila(id, valor, token, linea, var_type, scope, id_type)
    nuevo_nodo.next = self.sup
    self.sup = nuevo_nodo

  def pop(self):
    sup = self.sup
    self.sup = self.sup.next
    return sup

  # retorna el nodo
  def buscar(self, elem, scope):
    if self.sup == None:
      print("No hay elementos de la pila")
      return
    temp = self.sup
    while temp != None:
      if temp.valor == elem and temp.scope == scope:
        return temp
      temp = temp.next

  def __str__(self) -> str:
    temp = self.sup
    print("PILA SIMBOLOS")
    while temp != None:
      print(f"{temp.id} - {temp.valor} - {temp.scope}")
      temp = temp.next

  def imprimir(self):
    temp = self.sup
    print("PILA SIMBOLOS")
    while temp != None:
      print(f"{temp.id} - {temp.valor} - {temp.scope}")
      temp = temp.next
