class Generador:
  str_data = ".data\n"
  str_body = "\n.text\n\nmain:\n\n"#Se agrego .text y con ello SI compila

  def __init__(self, pila_var, name_fx, nodo, nodo_fin) -> None:
    self.pila_var = pila_var
    self.name_fx = name_fx
    self.nodo = nodo
    self.nodo_fin = nodo_fin

    self.gen_var_global()#Variables en data
    self.gen_code()#Codigo en main
    #La salida se escribe en el analizador semantico

  def gen_var_global(self):#ok
    for i in self.pila_var:
      if i.tipo_dato == "int_type": #La variable es entera
        Generador.str_data += f"\t var_{i.lex}_{self.name_fx}: \t .word 0:1\n"

      elif i.tipo_dato == "string_type": #La variable es una cadena/string
        Generador.str_data += f'\t str_{i.lex}_{self.name_fx}: \t .asciiz " "\n'

  def gen_code(self):
    nodo = self.nodo.next_terminal_stop(self.nodo_fin)
    linea = nodo.line

    while nodo != None and nodo.id != 0:#Reusamos la estructura de type_checker
      
      if nodo.padre.symbol == "ID_STX": #Bucle "para" tambien usa =
        self.gen_ID_STX(nodo)

      elif nodo.symbol == "if":
        varl = [ nodo.next_terminal() ]
        varl.append(varl[0].next_terminal().next_terminal())
        bool_oper = varl[0].next_terminal().symbol

        stack_stx = list()
        for i in range(2):
          es_var = False
          type_va = varl[i].symbol
          if varl[i].symbol == "id":
            es_var = True
            type_va = self.get_type(varl[i].lex)

          stack_stx.append( {"lex":varl[i].lex, "var":es_var, "type": type_va, "oper":None} )

        self.gen_load_var_val_master(stack_stx)
        self.gen_oper_if(bool_oper)
        Generador.str_body += "\nlabel_true:\n"

        nodo = nodo.next_terminal_stop(self.nodo_fin)
        while nodo != None and nodo.id != 0:
          if nodo.padre.symbol == "ID_STX":
            self.gen_ID_STX(nodo)

          elif nodo.symbol == "else":
            Generador.str_body += "b label_end \n\nlabel_false: \n"
            break

          nodo = nodo.next_terminal_stop(self.nodo_fin)

        if nodo.symbol == "else":
          while nodo != None and nodo.id != 0:
            if nodo.padre.symbol == "ID_STX":
              self.gen_ID_STX(nodo)

            elif nodo.symbol == "finll":
              Generador.str_body += "\nlabel_end:\n"
              break

            nodo = nodo.next_terminal_stop(self.nodo_fin)

      nodo = nodo.next_terminal_stop(self.nodo_fin)

  def gen_load_var_val_master(self, stack_stx):
    for i in range(len(stack_stx)):
      dat = stack_stx[i]
      ultimo = False
      if i == (len(stack_stx) - 1):
        ultimo = True

      if dat["var"] == True:
        self.gen_load_var(dat["lex"], ultimo)

      else:
        self.gen_load_valor(dat["lex"], ultimo)
  
  def gen_oper_master(self, stack_stx):
    for i in stack_stx:
      self.gen_oper(i["oper"])
  
  def gen_asignacion_master(self, nodo_id):
    Generador.str_body += f"la $t1, var_{nodo_id.lex}_{self.name_fx} \n"
    Generador.str_body += "sw $a0, 0($t1) \n\n"

  def gen_load_var(self, nombre, es_ultimo):
    Generador.str_body += f"la $a0, var_{nombre}_{self.name_fx} \n"

    if not es_ultimo:
      Generador.str_body += "sw $a0, 0($sp) \n"
      Generador.str_body += "addiu $sp, $sp, -4 \n"
    
    Generador.str_body += "\n"

  def gen_load_valor(self, valor, es_ultimo):
    valor = eval(valor)
    if type(valor) == float:
      Generador.str_body += f"li.d $a0, {valor} \n"

    elif type(valor) == int:
      Generador.str_body += f"li $a0, {valor} \n"

    if not es_ultimo:
      Generador.str_body += "sw $a0, 0($sp) \n"
      Generador.str_body += "addiu $sp, $sp, -4 \n"

    Generador.str_body += "\n"

  def gen_oper(self, oper):
    if oper:
      Generador.str_body += "lw $t1, 4($sp) \n"
      dict_op = {"plus":"add", "minus":"sub", "times":"mul"}#Las operaciones de division y modulo usan LO, HI. Tienen tratamiento especial
      Generador.str_body += f"{dict_op[oper]} $a0, $t1, $a0 \n"
      Generador.str_body += "addiu $sp, $sp, 4 \n\n"

  def gen_oper_if(self, oper):
    Generador.str_body += "lw $t1, 4($sp) \n"
    Generador.str_body += "addiu $sp, $sp, 4 \n"
    dict_op = {"igual_doble" : "bne", "no_igual" : "beq", "menor" : "bge", "menor_igual" : "bgt", "mayor" : "ble", "mayor_igual" : "blt"}
    Generador.str_body += f"{dict_op[oper]} $t1, $a0, label_false \n"
    

  def get_type(self, id_name):
    for i in self.pila_var:
      if id_name == i.lex:
        tipo = i.tipo_dato
        tipo = tipo.split("_")[0]
        return tipo

  def gen_ID_STX(self, nodo):
    nodo_punto_coma = nodo.next_symbol("punto_coma")
    stack_stx = list()
    foreach = nodo.next_terminal()

    while foreach != nodo_punto_coma:
      if foreach.symbol == "id":
        lam_PARAM_CALL_FX =  foreach.DFS_next().hijos[0].symbol # lam(var) || PARAM_CALL_FX (llamada a funcion)
        if lam_PARAM_CALL_FX == "lam":
          local_type = self.get_type(foreach.lex)
          stack_stx.append({"lex":foreach.lex, "var":True, "type": local_type, "oper":None})

      elif foreach.padre.symbol=="VALOR":
        local_type = foreach.symbol
        stack_stx.append({"lex":foreach.lex, "var":False, "type": local_type, "oper":None})

      elif foreach.padre.symbol == "OPER_MATH":
        stack_stx[-1]["oper"] = foreach.symbol

      foreach = foreach.next_terminal()

    nodo_id = nodo.padre.padre.hijos[0] # ""id"" = 3 + 4, obtenemos id

    #Cargamos las variables y valores en pila
    self.gen_load_var_val_master(stack_stx)

    #ALU
    self.gen_oper_master(stack_stx)

    #Asignacion
    self.gen_asignacion_master(nodo_id)

  @classmethod
  def write_output(cls):#ok
    with open("codigo.s", "w") as outcode:
      outcode.write(Generador.str_data + Generador.str_body)
    