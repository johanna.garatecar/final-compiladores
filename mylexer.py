# INTEGRANTES:
#
# - Johanna Garate Cardenas
# - Kristhyan A. Kurt Lazarte Zubia
#

# Ver 3.0 - Se ignoran los comentarios, los tipos de dato tienen sufijo TYPE

import ply.lex as lex

# r'atring' -> r significa que la cadena es tradada sin caracteres de escape,
# es decir r'\n' seria un \ seguido de n (no se interpretaria como salto de linea)

reserved = {
    'algoritmo': 'MAIN',
    'funcion': 'FUNCTION',
    'definir': 'DEFINE',
    'como': 'AS',
    'entero': 'INT_TYPE',
    'real': 'DOUBLE_TYPE',
    #'caracter': 'CHAR_TYPE',
    'cadena': 'STRING_TYPE',
    'logico': 'BOOL_TYPE',
    'arreglo': 'ARRAY_TYPE',
    'imprimir': 'PRINT',
    'leer': 'SCAN',
    'mientras': 'WHILE',
    'para': 'FOR',
    'hasta': 'UNTIL',
    'conpaso': 'SALTO',
    'si': 'IF',
    'entonces': 'THEN',
    'sino': 'ELSE',
    'sinosi': "ELSEIF",
    #'segun': 'SWITCH',
    #'predeterminado': 'DEFAULT_SWITCH',
    'y': 'AND',
    'o': 'OR',
    'no': 'NEGACION',
    'mod': 'MOD',
    'devolver': 'RETURN'
}

# List of token names.   This is always required
tokens = [
    'COMENTARIO', 'COMENTARIO_LARGO', 'DOUBLE', 'INT', 'BOOL', 'STRING',
    'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'INIPAR', 'FINPAR',
    'INILL', 'FINLL', 'INICOR', 'FINCOR', 'PUNTO_COMA',
    'COMA', 'DOSPTS', 'COMILLAS_DOBLE', 'IGUAL',
    'NO_IGUAL', 'IGUAL_DOBLE', 'MAYOR', 'MENOR', 'MAYOR_IGUAL', 'MENOR_IGUAL',
    'POTENCIA', 'id'
]  # + list(reserved.values())

tokens += reserved.values()
# Regular expression rules for simple tokens
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_INIPAR = r'\('
t_FINPAR = r'\)'
t_INILL = r'\{'
t_FINLL = r'\}'
t_INICOR = r'\['
t_FINCOR = r'\]'
t_PUNTO_COMA = r'\;'
t_COMA = r'\,'
#t_DOSPTS = r'\:'
t_COMILLAS_DOBLE = r'\"'
#t_COMILLAS_SIMPLE = r'\''
t_IGUAL = r'\='
t_NO_IGUAL = r'\!\='
t_IGUAL_DOBLE = r'\=\= '
#t_RETURN = r'\<\-'
t_MAYOR = r'\>'
t_MENOR = r'\<'
t_MAYOR_IGUAL = r'\>\='
t_MENOR_IGUAL = r'\<\='
t_POTENCIA = r'\^'

# A regular expression rule with some action code


def t_COMMENT(t):
    r'\/\/.*'
    pass


def t_COMENTARIO_LARGO(t):
    r'(/\*(.|\n)*?\*/)|(//.*)'
    pass

def t_DOUBLE(t):
    r'[-]?\d+\.\d*'
    #r'[-+]?[0-9]+(\.([0-9]+)?([eE][-+]?[0-9]+)?|[eE][-+]?[0-9]+)'
    t.value = float(t.value)
    return t

def t_INT(t):
    r'[-]?\d+'
    t.value = int(t.value)  # guardamos el valor del lexema
    #print("se reconocio el numero")
    return t


#Identificamos los id
def t_id(t):
    r'(\_ | [a-zA-Z])+ ( \w )*'
    if t.value in reserved:
        t.type = reserved[t.value]
    return t


# Booleanos
def t_BOOL(t):
    r'verdadero|falso'
    return t


def t_STRING(t):
    r'\".*\"'
    return t


#def t_CHAR(t):
#    r"\'.*\'"
#    return t


# Define a rule so we can track line numbers
def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)


# A string containing ignored characters (spaces and tabs)
t_ignore = ' \t'


# Error handling rule
def t_error(t):
    print("Illegal character '%s'" % t.value[0])
    t.lexer.skip(1)

# Build the lexer
lexer = lex.lex()

def exec_lexer(nameFile, outFile = "tokens_LL1.txt"):
    data = open(nameFile, "r").read()#.lower()
    

    # Give the lexer some input
    lexer.input(data)

    data_tokens = []
    # Tokenize
    while True:
        tok = lexer.token()
        if not tok:
            break  # No more input
        #(RETURN,'devolver', 4 linea, 94 pos en la linea )
        #print(tok.type, tok.value, tok.lineno, tok.lexpos)
        
        #Los lexemas en minusculas, excepto los string
        if tok.type == "STRING":
            my_dict = {'type':tok.type.lower(), 'lexeme':tok.value, 'line':tok.lineno}
        else:
            my_dict = {'type':tok.type.lower(), 'lexeme':str(tok.value).lower(), 'line':tok.lineno}
        #{'type': 'string', 'lexeme': '"Hola"', 'line': 10}
        data_tokens.append(my_dict)

    data_tokens.append({'type':'$', 'lexeme':'$', 'line':0})
    # {'type': 'finpar', 'lexeme': ')', 'line': 11}
    if outFile: #flag para guardar los tokens en fichero
        with open(outFile, "w") as outTokens:
            for i in data_tokens:
                if i["type"] == "$":
                    break
                
                outTokens.write(i['type']+ ' ')
    #for i in data_tokens:
    #    print(i)
    return data_tokens