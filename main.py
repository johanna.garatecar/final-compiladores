import os
import sys
import mylexer
from arbol_LL1 import *
from ana_semantic import *

def main():
  args = sys.argv[1:]
  codigoFuente = "data_basic2.pintc"#"data.pintc"
  tablaSemantica = "tabla_sintaxis.xlsx"

  if (len(args)):
    codigoFuente = args[0]

    if(len(args) == 2):
      tablaSemantica = args[1]
  
    elif ((len(args) > 2)):
      print("ERROR: Demasiados parametros")
      return

  try:
    os.remove("arbol_LL1.pdf")
    os.remove("arbol_LL1")
  except:
    pass

  ### RUN ###
  tokens = mylexer.exec_lexer(codigoFuente)

  arbol_gra = Arbol_LL1(tokens, tablaSemantica)
  arbol_gra.run()

  if arbol_gra.root == None:
    quit()
    
  print()
  
  semantic = Ana_semantic(arbol_gra.root) #Evidencia 3

  semantic.run()
  
  """
  pivot = arbol_gra.root
  while True:
    pivot = pivot.next_id()
    if pivot.id == 0:
      break
    print(pivot)

  """

if __name__=="__main__":
  main()