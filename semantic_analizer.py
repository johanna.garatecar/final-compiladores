from runner import Runner
import mylexer
import pilaid
from all_nodes import pilaArray
#self.asignacion_var_fun(root)
#self.fin_funcion(root, self.scope_pila[-1])
# scope
class AnalizadorSemantico:
  def __init__(self, arbol):
    self.arbol = arbol
    self.tabla_simbolos = pilaid.Pila()
    self.scope_pila = []

  def print_div(self, index):
    if index == 1:
      print()
      print("---------------------------------------")
    if index == 2:
      print("---------------------------------------")
      print()

  def print_pila(self):
    for i in self.scope_pila:
      print(i, end = " ")
    print()
    
  def buscar_id(self, nodo):
    if nodo.symbol == 'id':
      print("Variable encontrada: ", nodo.symbol, " - ", nodo.lexeme)
      self.tabla_simbolos.push(nodo.lexeme, nodo.symbol, nodo.line, 'parametro', self.scope_pila[-1], 'var', "")
      self.tabla_simbolos.imprimir()
      return
    for child in nodo.children:
      self.buscar_id(child)
  
  def definicion_var(self, root):
    if root.symbol == 'define':
      nodo_id = root.father.children[1]
      self.buscar_id(nodo_id, "Variable definida: ")
        
    for child in root.children:
      self.definicion_var(self, child)
  
  '''def definicion_fun(root):
    if root.symbol == 'EXPRES8':
      if root.children[0].symbol == 'id' and root.children[1].children[0].symbol == 'ini_parentesis':
        nodo = root.children[0]
        print("Función encontrada: ", nodo.symbol, " - ", nodo.lexeme)
        
    for child in root.children:
      definicion_var(child)'''

  def definicion_en_fun(self, root):
    self.tabla_simbolos.push(root.children[1].lexeme, root.children[1].symbol, root.children[1].line, 'return', self.scope_pila[-1], 'var', "")
    self.tabla_simbolos.imprimir()
    self.buscar_id(root.children[5])
    return
  
  def uso_var(self, root):
    if root.symbol == 'EXPRES8':
      if root.children[0].symbol == 'id' and root.children[1].children[0].symbol == 'ep':
        print("Variable usada: ", root.children[0].symbol, " - ", root.children[0].lexeme)
  
    for child in root.children:
      self.uso_var(child)

  def buscar_asignacion(self, root, asig):
    if root.type == 'hoja' and root.symbol != 'ep':
      print("Hoja y no ep:", root.lexeme)
      print("Asignacion = ", asig)
      asig += str(root.lexeme)
      print("Asignacion = ", asig)
    for child in root.children:
      self.buscar_asignacion(child, asig)

  def asignacion_var_fun(self, root):
    if root.symbol == 'EXPRES3' and root.children[0].children[0].children[0].children[0].children[0].children[0].symbol == 'id' and root.children[1].children[0].children[0].symbol == 'igual':
      nodo = root.children[0].children[0].children[0].children[0].children[0].children[0]
      symbol = self.tabla_simbolos.buscar(nodo.lexeme, self.scope_pila[-1])
      print("Símbolo: ", symbol.id, symbol.valor)
      self.buscar_asignacion(root.children[1].children[1], symbol.asignacion)
      print("Símbolo: ", symbol.asignacion)
    for child in root.children:
      self.asignacion_var_fun(child)
  
  def fin_funcion(self, root, lexeme):
    if root.symbol == 'fin_llave' and root.father.symbol == 'FUN' and root.father.children[3].lexeme == lexeme:
      nodo = root.father.children[3]
      print("Función: ", nodo.lexeme, " terminó")
      while self.tabla_simbolos.sup != None and self.tabla_simbolos.sup.scope == lexeme:
        self.tabla_simbolos.pop()
        self.tabla_simbolos.imprimir()
      self.scope_pila.pop()
      self.print_pila()
    for child in root.children:
      self.fin_funcion(child, lexeme)
  
  def funcion(self, root, lexeme):
    if root.symbol == "FUN" and root.children[3].lexeme == lexeme:
      print("Funcion encontrada: ", root.children[3].symbol, " - ", root.children[3].lexeme)
      print("Symbol:", root.children[0].symbol, root.children[1].symbol, root.children[2].symbol, root.children[3].symbol)
      print("Lexema:", root.children[0].lexeme, root.children[1].lexeme, root.children[2].lexeme, root.children[3].lexeme)
      return self.definicion_en_fun(root), self.asignacion_var_fun(root)
  
    for child in root.children:
      self.funcion(child, lexeme)
  
  def scope(self, root, fun_flag):
    #print("estamos en el main")
    # Buscamos dentro del main
    # Si encuentra definición insertar en la tabla de símbolos 
    
    # Si encuentra asignación buscar en la tabla y colocar la asignación
      # Si la asignación no está definida anteriormente dentro de su scope dar error
    #Si encuentra una función debe buscar la función
    if root.symbol == 'EXPRES8':
      if root.children[0].symbol == 'id' and root.children[1].children[0].symbol == 'ini_parentesis':
        nodo = root.children[0]
        print("Función encontrada en el main: ", nodo.symbol, " - ", nodo.lexeme)
        #añadir función encontrada al scope
        self.scope_pila.append(nodo.lexeme)
        self.print_pila()
        #Buscar la función
        self.funcion(self.arbol, nodo.lexeme)
        # Dentro de la función agregar las variables definidas
        # Si se encuentra una asignación buscar en la tabla y colocar la asignación
          # Si la asignación no está definida anteriormente dentro de su scope dar error
        #fun_flag = True
    #Si la función encontrada ha terminado quitar las variables que tienen ese scope de la tabla de símbolos
    if fun_flag == True:
      self.print_div(1)
      self.fin_funcion(self.arbol, self.scope_pila[-1])
      self.print_div(2)
      self.tabla_simbolos.imprimir()
      print("Scope Pila:", self.scope_pila)
      fun_flag = False
    for child in root.children:
      self.scope(child, fun_flag)
  
  def scope_main(self, root):
    for child in root.children:
      if child.symbol == 'ALG':
        self.scope(child.children[3], False)
      else:
        self.scope_main(child)



#print("---- SCOPE -----")
#print("....... Variables Declaradas .......")
#definicion_var(ejecucion.root)
#print("\n....... Variables Usadas .......")
#uso_var(ejecucion.root)
#print("\n....... Término de Funciones .......")
#fin_funcion(ejecucion.root)