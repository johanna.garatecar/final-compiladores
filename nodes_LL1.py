from multiprocessing.sharedctypes import Value
from graphviz import Digraph

class node_stack:
  stack_counter = 0
  def __init__(self, valor, is_terminal):
    self.id = node_stack.stack_counter
    self.valor = valor
    self.is_terminal = is_terminal

    node_stack.stack_counter += 1

class node_parser:
  parser_counter = 0
  gra = Digraph()

  def __init__(self, symbol, type, line=None, lex = None, color=""):#is_terminal=None):
    self.symbol = symbol #Terminal o NO Terminal, valor
    self.type = type #Nodo u hoja
    self.hijos = [] #Hijos del arbol
    self.padre = None #Padre del nodo
    self.line = line # Linea en el código fuente
    #self.is_terminal = is_terminal #Bool
    self.lex = lex
    self.id = node_parser.parser_counter
    self.posHijo = -1

    if color:
      node_parser.gra.node(str(self.id), symbol, style="filled", fillcolor = color)

    else:
      node_parser.gra.node(str(self.id), symbol)

    node_parser.parser_counter += 1

  def addHijo(self, hijo):
    self.hijos.append(hijo)
    self.hijos[-1].padre = self
    self.hijos[-1].posHijo = len(self.hijos) - 1

    node_parser.gra.edge(str(self.id), str(self.hijos[-1].id))

  def DFS_next(self): #Busqueda del siguiente con DFS
    pivot = self
    while True:
      if pivot.id == 0:
        return pivot

      padre = pivot.padre

      i = pivot.posHijo + 1
      if i < len(padre.hijos):
        pivot = padre.hijos[i]
        return pivot

      pivot = pivot.padre

  def DFS_prev(self):
    pivot = self
    while True:
      if pivot.id == 0:
        return pivot

      padre = pivot.padre

      i = pivot.posHijo - 1
      if i > -1:
        pivot = padre.hijos[i]
        return pivot

      pivot = pivot.padre

  def next_terminal(self):
    pivot = self

    if pivot.id == 0:
      pivot = pivot.hijos[0]

    if pivot.type == "hoja":
      pivot = pivot.DFS_next()

    while True:
      if pivot.id == 0:
        return pivot
      if pivot.type == "hoja":
        if pivot.symbol == "lam":
          pivot = pivot.DFS_next()
        else:
          return pivot
      else:
        pivot = pivot.hijos[0]

  def next_terminal_stop(self, node_stop=None):
    pivot = self.next_terminal()

    if pivot.id == 0:
      return pivot

    if pivot == node_stop:
      return None

    return pivot

  def next_id_main(self):
    pivot = self
    pivot = pivot.next_terminal()
    while pivot.symbol != "id" and pivot.symbol != "main":
      pivot = pivot.next_terminal()
      if pivot.id == 0:
        return pivot
    return pivot

  def next_symbol(self, obj):
    pivot = self
    pivot = pivot.next_terminal()
    while pivot.symbol != obj:
      pivot = pivot.next_terminal()
      if pivot.id == 0:
        return pivot
    return pivot

  def next_symbol_stop(self, obj, node_stop=None):
    pivot = self
    pivot = pivot.next_terminal()
    while pivot.symbol != obj:
      pivot = pivot.next_terminal()
      if pivot.id == 0:
        return pivot

      if pivot == node_stop:
        return None
    return pivot

  def next_arr_symbol(self, arr):
    try:
      if type(arr) == list:
        raise ValueError
    except:
      print("El parametro arr debe ser un arreglo")
    pivot = self
    pivot = pivot.next_terminal()
    while not pivot.symbol in arr:
      pivot = pivot.next_terminal()
      if pivot.id == 0:
        return pivot

    return pivot


  def __str__(self) -> str:
    txtOut = "Symbol: {} | Type: {} | Line: {} | Lex: {}".format(self.symbol, self.type, self.line, self.lex)
    return txtOut
