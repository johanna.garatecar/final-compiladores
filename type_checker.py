from copy import deepcopy
from generador_codigo import Generador

class Type_checker:
  def __init__(self, var_stack) -> None:
    self.var_stack = deepcopy(var_stack)

    i = 0
    while i < (len(self.var_stack) - 1):
      if self.var_stack[i].var_fx == "fx":
        self.var_stack.pop(i+1)
        continue
      i += 1

    self.nodo = self.var_stack[i].nodo
    self.nodo_fin = self.var_stack[i].nodo_fin

    self.stack_fx = self.var_stack.pop(i)
    self.fx_lex = self.stack_fx.lex
    #print(f"El nombre de la función es: {self.fx_lex}")
    while i < len(self.var_stack):#Solo dejamos a las variables en la pila
      self.var_stack.pop(i)

    self.run()
    
    Generador(self.var_stack, self.fx_lex, self.nodo, self.nodo_fin)

  def get_type(self, lex_id):
    for i in self.var_stack:
      if i.lex == lex_id:
        return i.tipo_dato

  def run(self):
    pivot_fin = None
    pivot = self.nodo
    nodo_var = None
    while pivot:
      pivot = pivot.next_symbol_stop("igual", self.nodo_fin)
      if pivot == None or pivot.id==0:
        break
      if pivot.padre.symbol == "ID_STX":
        nodo_var = pivot.padre.padre.hijos[0]
        tipo_var = self.get_type(nodo_var.lex)
        pivot_fin = pivot.next_symbol("punto_coma")
        ele = pivot.next_terminal()

        while ele != pivot_fin:
          if ele.symbol == "id":
            es_call_fx = ele.DFS_next().hijos[0].symbol#Symbol del hijo de EXPR_ID
            if es_call_fx == "lam":
              ele_tipo_dato = self.get_type(ele.lex)
              if ele_tipo_dato != tipo_var:
                print(f'ERROR TYPE CHECKER, "{ele.lex}" no es del mismo tipo que "{nodo_var.lex}" en linea {ele.line}')
                quit()
            else:
              break

          elif ele.padre.symbol=="VALOR":
            ele_tipo_dato = ele.symbol
            if ele_tipo_dato != tipo_var.split("_")[0]:
                print(f'ERROR TYPE CHECKER, "{ele.lex}" no es del mismo tipo que "{nodo_var.lex}" en linea {ele.line}')
                quit()

          ele = ele.next_terminal()

    print(f"Type Checker superado en función {self.nodo.lex}!")