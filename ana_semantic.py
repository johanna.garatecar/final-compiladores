#from numpy import add_newdoc
from type_checker import *

class stack_semantic:
  def __init__(self, nodo, var_fx, nodo_fin=None):#, lex_fx_padre = "global") -> None:
    self.lex = nodo.lex
    self.var_fx = var_fx
    #self.lex_fx_padre = lex_fx_padre
    self.linea = nodo.line
    self.tipo_dato = None
    #self.valor = None
    self.nodo = nodo
    self.nodo_fin = nodo_fin

  def __str__(self) -> str:
    return f"{self.linea} -Lex: {self.lex} -Var_Fx: {self.var_fx} -tipo: {self.tipo_dato}"# -Fx padre: {self.lex_fx_padre}"

class Ana_semantic:
  def __init__(self, root) -> None:
    self.root = root
    self.pivot = root
    self.pivot_fx = None
    self.stack = []

  def __str__(self) -> str:
    strOut = "Stack:\n"
    for i in self.stack:
      strOut += str(i) + "\n"
    return strOut

  def printAll(self):
    pivot = self.root
    print(pivot)
    while True:
      if len(pivot.hijos):
        pivot = pivot.hijos[0]
      elif pivot.symbol == "lam":
        pivot = pivot.padre
        pivot = pivot.DFS_next()
      else: #pivot.type == "hoja"
        print(pivot)
        pivot = pivot.DFS_next()
      if pivot == self.root:
        return

  def add_nodo_fin(self,nodo_stack):
    nodo_fin = None
    if self.pivot.lex == "main":
      nodo_fin = self.pivot.DFS_next().DFS_next().DFS_next()

    else:
        nodo_fin = self.pivot.DFS_next().DFS_next().DFS_next().DFS_next().DFS_next().DFS_next()
    nodo_stack.nodo_fin = nodo_fin
  
  def add(self,var_fx):
    nodo = stack_semantic(self.pivot, var_fx)
    if var_fx == "fx":
      self.add_nodo_fin(nodo)
      if len(self.stack):
        typecheck = Type_checker(self.stack)
      self.remove()
    else:### Agregamos el tipo
      pivot = self.pivot
      ant = pivot.DFS_prev()
      #print(f"ANTERIOR Sym: {ant.symbol} {ant.lex}")
      if ant.symbol == "array_type":
        nodo.tipo_dato = ant.symbol
      else:
        while pivot.symbol != "as":
          pivot = pivot.next_terminal()
        pivot = pivot.DFS_next().hijos[0]
        nodo.tipo_dato = pivot.symbol

    self.stack.insert(0, nodo)
  
  def remove(self):
    #if self.pivot_fx:
    #  pass
    while len(self.stack):
      if self.stack[0].var_fx == "var":
        self.stack.pop(0)
      else:
        break

  def findStack(self):
    for i in self.stack:
      if i.lex == self.pivot.lex:
        return i.var_fx
    return False

  def is_def(self):
    padre = self.pivot.padre
    if padre.symbol in ["DEF_FX", "DEFINIR_FX", "PARAM", "PARAMS", "PARA", "DEFINIR"]:
      return "var"
    elif padre.symbol == "FX":
      return "fx"
    return False

  def find(self):
    rp_find = self.findStack()
    creation = self.is_def()
    return rp_find, creation

  def run(self):
    while True:
      self.pivot = self.pivot.next_id_main()
      if self.pivot == self.root:
        typecheck = Type_checker(self.stack)
        print("Scope y type checker completados con exito!\n")
        Generador.write_output() ### Escribimos el código fuente! ###
        #print(self)
        return

      if self.pivot.symbol == "main":
        self.add("fx")
        continue

      rp_find,creation = self.find()# var | fx | False

      if (creation):
        if (rp_find != False and ( rp_find!="var" or creation!="fx" ) ):
          print(f"ERROR en la linea {self.pivot.line}, {self.pivot.lex} ya esta definido")
          quit()
        self.add(creation)

      else: #Se está usando la variable
        if rp_find == False: #No está en la pila y no está definida
          print(f"ERROR en la linea {self.pivot.line}, {self.pivot.lex} no fue definida")
          quit()

        #Verificamos si es llamada a funcion o uso de variable
        uso_var_fx = "var"
        try:
          pivot = self.pivot.padre
          pivot = pivot.hijos[1].hijos[0]
          if pivot.symbol == "PARAM_CALL_FX":
            uso_var_fx = "fx"
        except:
          pass

        if uso_var_fx != rp_find:
          print(f"ERROR en la linea {self.pivot.line}, {self.pivot.lex} tiene un uso indebido")
          quit()
        #print(f'Se está usando el id "{self.pivot.lex}" en linea {self.pivot.line}')
      #print(self)

  def printStack(self):
    print(self)