from turtle import fillcolor
import pandas as pd
import graphviz

from nodes_LL1 import *

class Arbol_LL1:
  def __init__(self, tokens, nombreFile) -> None:
    
    self.tablaSintax = pd.read_excel(nombreFile, index_col=0)

    self.terminales = list(self.tablaSintax.columns.values)[1:]

    self.stack = [node_stack("$", True)]
    self.stack.append(node_stack("E", False))
    #El primer no terminal de inicio

    self.tokens = tokens
    self.root = node_parser(self.stack[1].valor, "nodo")
    self.pivot = self.root

    self.img = graphviz.Digraph("arbolSintax")

  def is_terminal(self, val):
    return val in self.terminales

  def is_nan(self, val):
    return type(val) == float

  def run(self):
    while True:
      ultStack = self.stack[-1]
      if ultStack.valor == "$" and self.tokens[0]["type"] == "$":
        print("Validación con la gramática exitosa")
        break

      if ultStack.is_terminal:
        if ultStack.valor == self.tokens[0]["type"]:
          self.stack.pop()    #ultPila
          ultToken = self.tokens.pop(0)  #primero de los tokens

          self.pivot.line = ultToken["line"]
          self.pivot.lex = ultToken["lexeme"]
          #######################
          self.DFS_next()
          
        else:
          print("ERROR LL1: TERMINAL")
          self.root = None
          quit()

      else:
        self.update()

    node_parser.gra.render("arbol_LL1")

  def update(self):
    valor = self.stack[-1].valor
    token_type = self.tokens[0]['type']

    action = self.tablaSintax.loc[valor][token_type]

    if self.is_nan(action):
      print("Error LL1: No ubicado en la tabla de sintaxis")
      print(f"{valor} {token_type}")
      self.root = None
      quit()

    else:
      ele = action.split(" ")[::-1]
      # Separamos elementos en lista y revertimos
      if ele[0] == "lam":
        ultPila = self.stack.pop()

        #CODIGO ARBOL
        hojaLam = node_parser("lam", "hoja", -1)
        self.pivot.addHijo(hojaLam)
        self.DFS_next()

      else:
        #Reemplazamos el ultimo simbolo de la pila
        ultPila = self.stack.pop()

        for i in ele:
          nodo_pila = node_stack(i, self.is_terminal(i))
          self.stack.append(nodo_pila)

        #CODIGO ARBOL
        ele = ele[::-1]
        for i in ele:
          is_terminal = self.is_terminal(i)
          tipo = "nodo"
          nodo = None

          if (is_terminal):
            tipo = "hoja"
            nodo = node_parser(i, tipo, line=None, color='yellow')
          else:
            nodo = node_parser(i, tipo, line=None)
          #Insertamos al arbol
          
          self.pivot.addHijo(nodo)
        
        self.pivot = self.pivot.hijos[0]

  def DFS_next(self):#Busqueda en profundidad
    self.pivot = self.pivot.DFS_next()
